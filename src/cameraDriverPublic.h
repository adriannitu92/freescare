#ifndef __CAMERA_DRIVER_PUBLIC__
#define __CAMERA_DRIVER_PUBLIC__

#include "MPC5604B_M27V.h"
#include "constants.h"

void CameraInit(void);
/*/ Functia CameraRead o sa scrie in variabilele CameraData[128] si center DIN MAIN/*/
uint8_t CameraRead(void);
uint8_t CameraReadD(void);
void CameraDelay(void);
void Servo(void);


#endif
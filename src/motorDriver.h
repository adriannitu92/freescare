#ifndef __MOTOR_DRIVER__
#define __MOTOR_DRIVER__

#include "MPC5604B_M27V.h"
#include "constants.h"

void MotorsInit(void);
void Servo(void);

#endif
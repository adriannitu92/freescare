#include "cameraDriverPublic.h"
#include "cameraDriverPrivate.h"

uint32_t delayForCamera = 200;
uint8_t max, min, plaja, smax;
uint8_t Result[128];
uint8_t madness;

extern uint8_t CameraData[128];
uint8_t Ccenter;
uint8_t skip = 0 ;

uint8_t centers[CENTERBUFFERSIZE] = {0};
uint8_t pos = 0;
unsigned int Csum = 0;

void CameraDelay(void){
	uint32_t i;
	for(i=0;i<delayForCamera;i++)
		CameraClock();
}

void initADC(void) {
	//ADC.MCR.R = 0x20020000;         	/* Initialize ADC scan mode*/
	ADC.MCR.R = 0x00000000;         	/* Initialize ADC one shot mode*/
	ADC.NCMR[0].R = 0x00000007;      	/* Select ANP1:2 inputs for normal conversion */
	ADC.CTR[0].R = 0x00008606;       	/* Conversion times for 32MHz ADClock */
}

void CameraClock(void){
	uint8_t i;
	for(i=0;i<15;i++)
  		;
}

void CameraInit(void) {
	uint8_t i;
	initADC();
	for(i = 0; i < 128; i++)
		CameraData[i] = 0;
	for(i = 0; i < CENTERBUFFERSIZE; i++)
	{
		Csum +=64;
		centers[i] = 64;
	}
	Ccenter =0;
	max = 0;
	delayForCamera = 200;
	CameraRaw();
	i = 0;
	CameraDelay();
	while(smax <= 200 ) {
		uint4_on_LEDs(i++);	
		CameraReadD();
		delayForCamera += 20;
	}
}

void minMax(void)
{
	uint8_t i;
	uint8_t done;
	min = 255;
	max = 0;
	done = 0;;
	smax = 0;
	for(i = 0; i < 128; i+=2) // ignoram edges, sa evitam poisoning
	{
		if(Result[i] > Result[i+1])
		{
			if(Result[i] > max) {
				max = Result[i];
				if(!done)
					smax = max; 
			}
			if(Result[i+1] < min)
				min = Result[i+1];
		}
		else
		{
			done = 1;			
			if(Result[i+1] > max)
				max = Result[i+1];
			if(Result[i] < min)
				min = Result[i];
		}
	}// 3n/2 comparatii in loc de 2n comparatii
	plaja = max - min;
}

void NORMARE(void)
{
	uint8_t i;
	float factor;
	// acum am putea sa normam Result[i] pentru a stabiliza putin datele
	minMax();
	if(plaja == 0)
	{
		//while(1)
		//	;
	}
	factor = 255.0f/plaja;	
	for(i = 0; i < 128; i++)
	{
		Result[i] = (Result[i] - min)* factor ;
	}
}


void CameraRaw(void)
{
	uint8_t i;
	uint32_t adcdata;
	SIU.PCR[27].R = 0x0200;				/* Program the Sensor read start pin as output*/
	SIU.PCR[29].R = 0x0200;				/* Program the Sensor Clock pin as output*/
	
	// page 222 in reference
	SIU.PGPDO[0].R &= ~0x00000014;		/* All port line low */
	SIU.PGPDO[0].R |= 0x00000010;		/* Sensor read start High */
	CameraClock();
	SIU.PGPDO[0].R |= 0x00000004;		/* Sensor Clock High */
	CameraClock();
	SIU.PGPDO[0].R &= ~0x00000010;		/* Sensor read start Low */ 
	CameraClock();
	SIU.PGPDO[0].R &= ~0x00000004;		/* Sensor Clock Low */
	CameraClock();
	for (i=0;i<128;i++)
	{
		CameraClock();
		SIU.PGPDO[0].R |= 0x00000004;	/* Sensor Clock High */
		ADC.MCR.B.NSTART=1;     		/* Trigger normal conversions for ADC0 */
		while (ADC.MCR.B.NSTART == 1) {};
		adcdata = ADC.CDR[0].B.CDATA;
		CameraClock();
		SIU.PGPDO[0].R &= ~0x00000004;	/* Sensor Clock Low */
		Result[i] = (uint8_t)(adcdata >> 2);
	}
}

void CONVERT(void)
{
	uint8_t i;
	uint32_t aux;
	for(i=2; i < 126; i++)//5 step
	{
		aux = ((int32_t)Result[i]) * 16 - ( ((int32_t) Result[i-2]) + ((int32_t)Result[i+2]));
		CameraData[i] = 0; //TODO -- e un caz special aici, ask Adrian
		if(aux > 255)
			CameraData[i] = 255;
		if(aux < 0)
			CameraData[i] = 0;
	}
	
}


void CENTERDETECTION(void) //TODO -- this all MUST be updated before play to take account of 2 lines!!!
{
	//sanity check
	uint8_t lines;
	uint8_t edges;
	uint8_t i;
	uint8_t first, last;
	uint8_t leftEdge[10];
	uint8_t rightEdge[10];
	uint8_t edgeIndex = 0;
	uint8_t nextCenter = 0;
	uint8_t oldEdgeError = 255;
	uint8_t found = 0;
	uint8_t first_flag = 1;
	first = 0;
	last = 127;
	lines = 0;
	madness = 0;
	edges = 0;
	for(i = 20 ; i < 108; i++)
	{
		if(CameraData[i] != CameraData[i+1])
		{
			edges ++;
			if(!first_flag) {
				first = i;
				first_flag = 0;
				if(edgeIndex < 10) {
					leftEdge[edgeIndex] = i;
				}
			}
			else {
				first_flag = 0;
				last = i;
				if(edgeIndex < 10) {
					rightEdge[edgeIndex] = i;
					edgeIndex++;
				}
			}
		}
	}
	if(edges % 2 )
		madness = 1;
	lines = edges /2;	
	if(lines != 1)
		madness = 1;
	if (! first || last == 127)
		madness = 1;
	// sanity check end
	
	if (madness)
	{
		
	}	
	else
	{	
		float val;	
		int newCenter  = first/2 + last/2 + (first %2 + last%2)/2;
		val = Csum/CENTERBUFFERSIZE;
		//if( modulus (val - newCenter) < 100 )
		{
			Csum -= centers[pos%CENTERBUFFERSIZE];
			centers[pos%CENTERBUFFERSIZE] = newCenter;
			Csum += newCenter;
			pos++;
			Ccenter = newCenter;
		}
		//Ccenter  = first/2 + last/2 + (first %2 + last%2)/2;
	}
}

unsigned int modulus(int x)
{
	if(x < 0)
		return -x;
	return x;
}

uint8_t CameraRead(void)
{
	CameraRaw();
	NORMARE();
	CONVERT();
	CENTERDETECTION();
	return Ccenter;
}

uint8_t CameraReadD(void)
{
	CameraRaw();
	NORMARE();
	CONVERT();
	CENTERDETECTION();
	CameraDelay();
	return Ccenter;
}

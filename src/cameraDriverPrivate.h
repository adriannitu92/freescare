#ifndef __CAMERA_DRIVER_PRIVATE__
#define __CAMERA_DRIVER_PRIVATE__

#include "MPC5604B_M27V.h"
#include "constants.h"

void CameraClock(void);
void minMax(void);
void NORMARE(void);
void CameraRaw(void);
void CONVERT(void);
void CENTERDETECTION(void);
void MADNESS(int err);
unsigned int modulus(int x);

void uint4_on_LEDs(uint8_t ledValue);

#endif
#ifndef __CONSTANTS__
#define __CONSTANTS__

#define KU 6.0f
#define TU 1.0f

#define USE_SQUARES 1

#define CENTERBUFFERSIZE 8

#define BUFFERSIZE 25
#define MAXQUADSUM 3750

#define SPEEDSTRAIGHT 800
#define SPEEDCURVE 500

#define SPEEDBOOSTUP 900
#define SPEEDBOOSTDOWN 250
#define SPEEDBOOSTDOWNER 0

#define BOOSTFRAMES 30
#define BOOSTFRAMESBRAKE (40)
//50 frames = 1 secunda 

#define SERVO_CENTER 990
#define SERVO_SWING 275

#define MAXACC (500)

#endif
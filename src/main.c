
#include "MPC5604B_M27V.h"
#include "motorDriver.h"
#include "cameraDriverPublic.h"

uint32_t curveSpeed = SPEEDCURVE ;

volatile uint8_t CameraData[128];
volatile uint8_t center;
extern float servoValue;
int errPrev = 0;
double acc = 0;

uint8_t usingLeds = 0;

static void InitHW(void);
static void InitSysclk(void);
static void InitModesAndClks(void);
static void InitPeriClkGen(void);

uint8_t tickFinishLine = 2;

static void DisableWatchdog(void);
static void InitGPIO(void);
static void InitPIT3(void);

static void PIT3_isr(void);
static void RGM_ipi_isr(void);

int64_t History[BUFFERSIZE] = {0};
uint8_t position = 0 ;
uint64_t suma = 0;
uint64_t maxQuadSum = MAXQUADSUM;

uint8_t curveFlag = 0;
uint8_t straightFlag = 1;
uint8_t straight = BOOSTFRAMES;
uint8_t curve = 0;

uint8_t speedCounter = 0;
uint8_t pCounter = 0;
uint8_t iCounter = 0;
uint8_t dCounter = 0;

float straightCounter = 0;


volatile float KP = 0.0f;
volatile float KI = 0.0f;
volatile float KD = 0.0f;
volatile float speedKP = 0.5f * 8;
volatile float speedKI = 0.0f;
volatile float speedKD = 0.0f;
volatile float curveKP = 0.5f * 8 ;
volatile float curveKI = 0.00005f * 2;
volatile float curveKD = 0.5f * 3;
volatile int SPEED = 250;
volatile int BoostRight = 0;
volatile int BoostLeft = 0;
volatile float speedError = 0.001;

uint8_t ready = 0 ;
 
static void InitHW(void)
{
    DisableWatchdog();
    InitSysclk();
    InitGPIO();  
}

void clock_setting()
{
    ME.MER.R = 0x0000001D;
    CGM.FMPLL_CR.R = 0x02400100;    	/* 8 MHz xtal: Set PLL0 to 64 MHz */   
    ME.RUN[0].R = 0x001F0074;
    ME.RUNPC[0].R = 0x00000010;
        /* Peripheral Operation in RUN3  mode: Disabled    */
        /* Peripheral Operation in RUN2  mode: Disabled    */
        /* Peripheral Operation in RUN1  mode: Disabled    */
        /* Peripheral Operation in RUN0  mode: Enabled    */
        /* Peripheral Operation in DRUN  mode: Disabled    */
        /* Peripheral Operation in SAFE  mode: Disabled    */
        /* Peripheral Operation in TEST  mode: Disabled    */
        /* Peripheral Operation in RESET  mode: Disabled    */


/* ----------------------------------------------------------- */
/*              Interrupt Setup            */
/* ----------------------------------------------------------- */

    ME.IM.R = 0x00000000;
        /* Invalid Mode Configuration Interrupt: Disabled    */
        /* Invalid Mode Interrupt: Disabled    */
        /* SAFE Mode Interrupt: Disabled    */
        /* Mode Transition Complete Interrupt: Disabled    */

/* ----------------------------------------------------------- */
/*              Operating Modes Initialization - End                */
/* ----------------------------------------------------------- */



 /* ----------------------------------------------------------- */
    /*              Enable All Peripheral Clocks            */
    /* ----------------------------------------------------------- */
    
        CGM.SC_DC[0].R =0x80;    
            /* Peripheral Set 1 Clock Divider Status: Enabled */
            /* Peripheral Set 1 Clock Divider: 1 */
        CGM.SC_DC[1].R =0x80;    
            /* Peripheral Set 2 Clock Divider Status: Enabled */
            /* Peripheral Set 2 Clock Divider: 1 */
        CGM.SC_DC[2].R =0x80;    
            /* Peripheral Set 3 Clock Divider Status: Enabled */
            /* Peripheral Set 3 Clock Divider: 1 */
  /* ----------------------------------------------------------- */
    /*                    System Output Clock Setup                 */
    /* ----------------------------------------------------------- */
    
        CGM.OCDS_SC.R = 0x12000000;    
            /* Clock Divider: 2 */
            /* Output Clock Source: System PLL */
        CGM.OC_EN.B.EN = 0x1;    
            /* Output Clock: Enabled */
    
    /* ------------------------128KHz Slow IRC Configuration------------------- */
        CGM.SIRC_CTL.R = 0x0000000;    
            /* LPRC Oscillator Trimming: 0x0 */
            /* The 128KHz RC Clock Divider = 1 */
            /* LPRC Oscillator is switched OFF in Standby Mode */
    
        
    /* ----------------------------------------------------------- */
    /*              Clock Source Configuration            */
    /* ----------------------------------------------------------- */
    
     
        CGM.FXOSC_CTL.R = 0x00800000;
            /* The End of Count Value for XOSC = 0x1    */
            /* The XOSC Clock Interrupt: Enabled    */
            /* The XOSC Clock Divider: 1    */
    
        //CGM.FMPLL_CR.R = 0x02280100;
            /* Input Division Factor: 1    */
            /* Output Division Factor: 8    */
            /* Loop Division Factor: 40    */
            /* Progressive Clock Switching Enabled in Standby Mode    */
    
        CGM.FMPLL_MR.R = 0x00010014;
            /* The selected Spread Type: Down Spread    */
            /* The Modulation Period for FMPLL: 1    */
            /* Frequency modulation for PLL: Disabled    */
            /* The Increment Step for FMPLL: 20    */
        ME.RUN[0].R = 
            /* Re-enter in RUN[0] mode to update the configuration */
        ME.MCTL.R = 0x40005AF0;    
            /* Mode & Key */
        ME.MCTL.R = 0x4000A50F;    
            /* Mode & Key  Inverted  */
	          	/* MPC56xxB/S EMIOS 0:  select ME.RUNPC[1] */
	                              		/* Mode Transition to enter RUN0 mode: */
        while(0x4 != ME.GS.B.S_CURRENTMODE){};    
        while(0x1 == ME.GS.B.S_MTRANS){}; 
}  

/*******************************************************************************
Function Name : InitSysclk
Engineer      : b08110
Date          : Feb-25-2010
Parameters    : NONE
Modifies      : NONE
Returns       : NONE
Notes         : initialize Fsys 40 MHz PLL with 8 MHz crystal reference
Issues        : NONE
*******************************************************************************/
static void InitSysclk(void)
{
#if 1
    InitModesAndClks();
    InitPeriClkGen();
#else
    clock_setting();
#endif
}

/*******************************************************************************
Function Name : InitModesAndClks
Engineer      : b08110
Date          : Aug-26-2011
Parameters    : NONE
Modifies      : NONE
Returns       : NONE
Notes         : - DRUN->RUN1->RUN0->RUN1
                - enable CLKOUT
Issues        : NONE
*******************************************************************************/
static void InitModesAndClks(void) 
{
    int32_t cnt = 0;
    
    ME.MER.R = 0x0000003D;        /* Enable DRUN, RUN0, RUN1 SAFE, RESET modes */
    
    /* Mode Transition to enter RUN1 mode: */    
    ME.RUN[1].R = 0x001F0030;       /* RUN1 cfg: 16MHzIRCON,OSC0ON,PLL0OFF,PLL1OFF,syclk=16MIRC */            
    ME.MCTL.R = 0x50005AF0;         /* Enter RUN1 Mode & Key */        
    ME.MCTL.R = 0x5000A50F;         /* Enter RUN1 Mode & Inverted Key */
    while(0 == ME.GS.B.S_FXOSC) {};               /* Wait for mode entry to complete */
    while(1 == ME.GS.B.S_MTRANS) {}    /* Wait for mode transition to complete */
    while(5 != ME.GS.B.S_CURRENTMODE) {};       /* Check RUN1 mode has been entered */ 
    
    
    /* Initialize PLL before turning it on: */
    /* fsys = fcrystal*ndiv/idf/odf */
    /* fvco must be from 256 MHz to 512 MHz */
    /* we want fsys = 120 MHz. fvco = fsys*odf = 120 MHz * 4 = 480 MHz */
    /* fsys =  40*72/6/4 = 120 MHz */

#if 1
    /* 40 MHz */
    CGM.FMPLL_CR.B.IDF = 0x0;    /* FMPLL0 IDF=0 --> divide by 1 */
    CGM.FMPLL_CR.B.ODF = 0x2;    /* FMPLL0 ODF=2 --> divide by 8*/
    CGM.FMPLL_CR.B.NDIV = 40;    /* FMPLL0 NDIV=40 --> divide by 40 */
#else
    
#endif
    CGM.FMPLL_CR.B.EN_PLL_SW = 1; 	/* enable progressive clock switching */ 
    
    
    ME.RUNPC[0].R = 0x000000FE; /* enable peripherals run in all modes */
    //ME.LPPC[0].R = 0x00000000;  /* disable peripherals run in LP modes */

#if 0    
    ME.RUN[0].R = 0x001F0032;       /* RUN0 cfg: 16MHzIRCON,OSC0ON,syclk=OSC */       
    
    /* Mode Transition to enter RUN0 mode: */
    ME.MCTL.R = 0x40005AF0;         /* Enter RUN0 Mode & Key */
    ME.MCTL.R = 0x4000A50F;         /* Enter RUN0 Mode & Inverted Key */  
    while (ME.IS.B.I_MTC != 1) {}    /* Wait for mode transition to complete */    
    ME.IS.R = 0x00000001;           /* Clear Transition flag */
#endif
    
    /* Mode Transition to enter RUN0 mode: */
    ME.RUN[0].R = 0x001F0070;       /* RUN0 cfg: 16MHzIRCON,OSC0ON,PLL0ON,syclk=16M IRC */    
    ME.MCTL.R = 0x40005AF0;         /* Enter RUN0 Mode & Key */
    ME.MCTL.R = 0x4000A50F;         /* Enter RUN0 Mode & Inverted Key */      
    while (1 == ME.GS.B.S_MTRANS) {}    /* Wait for mode transition to complete */
    while(4 != ME.GS.B.S_CURRENTMODE) {};       /* Check RUN0 mode has been entered */ 
    
    /* Mode Transition to enter RUN1 mode: */    
    ME.RUN[1].R = 0x001F0074;       /* RUN1 cfg: 16MHzIRCON,OSC0ON,PLL0ON,syclk=PLL0 */            
    ME.MCTL.R = 0x50005AF0;         /* Enter RUN1 Mode & Key */        
    ME.MCTL.R = 0x5000A50F;         /* Enter RUN1 Mode & Inverted Key */
    while(1 == ME.GS.B.S_MTRANS) {}    /* Wait for mode transition to complete */
    while(5 != ME.GS.B.S_CURRENTMODE) {};       /* Check RUN1 mode has been entered */ 
    
    /* enable CLKOUT on PA0 */
    /* AF2 - PCR[0] - PA = 0b10 */
    SIU.PCR[0].R = 0x0A00;
    
    /* set CLKOUT divider of 4 */
    CGM.OCDS_SC.R = 0x22000000; /* div by 4, system FMPLL */ 
    CGM.OC_EN.B.EN = 1; 		/* enable CLKOUT signal */
}

/*******************************************************************************
Function Name : InitPeriClkGen
Engineer      : b08110
Date          : Apr-14-2010
Parameters    : NONE
Modifies      : NONE
Returns       : NONE
Notes         : - Enable auxiliary clock 0-3. from PLL0
Issues        : NONE
*******************************************************************************/
static void InitPeriClkGen(void) 
{
    CGM.SC_DC[0].R = 0x80;  /* MPC56xxB: peri set 1 */
    CGM.SC_DC[1].R = 0x80;  /* MPC56xxB: peri set 2 */
    CGM.SC_DC[2].R = 0x80;  /* MPC56xxB: peri set 3 */    
}


static void InitGPIO(void)
{  
	SIU.PCR[64].R = 0x0100;				/* Program the drive enable pin of S1 (PE0) as input*/
	SIU.PCR[65].R = 0x0100;				/* Program the drive enable pin of S2 (PE1) as input*/
	SIU.PCR[66].R = 0x0100;				/* Program the drive enable pin of S3 (PE2) as input*/
	SIU.PCR[67].R = 0x0100;				/* Program the drive enable pin of S4 (PE3) as input*/
 	
    /* enable CLKOUT on PA0 */
    /* AF2 - PCR[0] - PA = 0b10 */
    SIU.PCR[0].R = 0x0A00;
    
	SIU.PCR[68].R = 0x0200;				/* Program the drive enable pin of LED1 (PE4) as output*/
	SIU.PCR[69].R = 0x0200;				/* Program the drive enable pin of LED2 (PE5) as output*/
	SIU.PCR[4].R = 0x0200;				/* Program the drive enable pin of LED1 (PE4) as output*/
	SIU.PCR[70].R = 0x0200;				/* Program the drive enable pin of LED3 (PE6) as output*/
	SIU.PCR[71].R = 0x0200;				/* Program the drive enable pin of LED4 (PE7) as output*/
}

static void InitPIT3(void)
{
	/* 30: MDIS = 0 to enable clock for PITs. */
    /* 31: FRZ = 1 for Timers stopped in debug mode */
    PIT.PITMCR.R = 0x00000001;    
    PIT.CH[3].LDVAL.R = 800000 - 1; //40MHZ /800k => 50 hz
    	
    /* clear the TIF flag */
    PIT.CH[3].TFLG.R = 0x00000001;
    	
    /* 30: TIE = 1 for interrupt request enabled */
    /* 31: TEN = 1 for timer active */
    PIT.CH[3].TCTRL.R = 0x00000003;
}
static void PIT3_isr(void)
{
	int eroare = center - 64;
	int correctSpeed;
	float oldValue;
	float positiveServo;
	int d;
	d = eroare - errPrev;
	if(errPrev != eroare)
		errPrev = eroare;	
	acc += eroare;
		servoValue = KP * eroare + KI * acc + KD * d;		
	if(servoValue > SERVO_SWING)
	{
		servoValue = SERVO_SWING;
	}
	else if(servoValue < -SERVO_SWING)
	{
		servoValue = -SERVO_SWING;
	}
	if(KI) {
		if(acc > MAXACC / KI)
			acc = MAXACC/KI;
		if(acc < -MAXACC / KI)
			acc = -MAXACC/KI;	
	}
	else
		acc = 0;
	
	if(suma <= maxQuadSum) 
	{
		if(curveFlag)
		{
			straight = BOOSTFRAMES;
			curveFlag = 0;
			straightCounter = 0;
		}
		if(straight)
		{
			SPEED = SPEEDBOOSTUP;
			straight--;
		}
		else
			SPEED = SPEEDSTRAIGHT ;
		KI = speedKI;
		KP = speedKP;
		KD = speedKD;
		straightFlag = 1;
		straightCounter +=0.02; //intreruperea e la 50hz :D
		if(!usingLeds)
		{
			SIU.GPDO[68].R = 1;	
			SIU.GPDO[69].R = 0;	
			SIU.GPDO[70].R = 0;
			SIU.GPDO[71].R = 1;	
		}
	} 
	else 
	{
		if(straightFlag)
		{
			if(straightCounter > 0.8)//daca am accelerat mai mult de 2 secunde
				curve = BOOSTFRAMESBRAKE;
			else
				curve = BOOSTFRAMES;
			straightFlag = 0;
		}
		if(curve)
		{
			if(straightCounter > 0.8)//daca am accelerat mai mult de 2 secunde
				SPEED = SPEEDBOOSTDOWNER;
			else
				SPEED = SPEEDBOOSTDOWN;
			curve--;
		}
		else
			SPEED = SPEEDCURVE;
		curveFlag = 1;
		KI = curveKI;
		KP = curveKP;
		KD = curveKD;
		if(!usingLeds)
		{
			SIU.GPDO[68].R = 1;		
			SIU.GPDO[69].R = 1;	
			SIU.GPDO[70].R = 1;
			SIU.GPDO[71].R = 0;
			if(servoValue > 0)
			{
				SIU.GPDO[68].R = 0;	
				SIU.GPDO[71].R = 1;
			}
		}
	}
	suma -= History[position];
	History[position] = servoValue * servoValue /16;
	suma += History[position ++];
	position = position % BUFFERSIZE;
	EMIOS_0.CH[4].CBDR.R = SERVO_CENTER + servoValue;
	if(servoValue > 150)
	{
		BoostLeft = - SPEED/2;
		BoostRight = SPEED/4;
	}
	else if (servoValue < -150)
	{
		BoostRight = - SPEED/2;
		BoostLeft = SPEED/4;
	}
	else{
		BoostRight = 0;
		BoostLeft = 0;
	}
    PIT.CH[3].TFLG.R = 0x00000001;       
}




static void DisableWatchdog(void)
{
    SWT.SR.R = 0x0000c520;     /* Write keys to clear soft lock bit */
    SWT.SR.R = 0x0000d928; 
    SWT.CR.R = 0x8000010A;     /* Clear watchdog enable (WEN) */
}

/*******************************************************************************
* Global functions
*******************************************************************************/
void Delaylong(void){
	int32_t dly;
  for(dly=0;dly<20000;dly++)
  {
  }
}

void Delaylonglong(void){
int lly;
  for(lly=0;lly<10;lly++) Delaylong();
}

void Delaycamera(void){
  Delaylonglong();
  Delaylonglong();
}


void uint4_on_LEDs(uint8_t ledValue) {
	uint32_t ledSettings = 0;
	
	SIU.GPDO[68].R = 1;
	SIU.GPDO[69].R = 1;
	SIU.GPDO[70].R = 1;
	SIU.GPDO[71].R = 1;
	
	if(ledValue & (1 << 0))
		SIU.GPDO[68].R = 0;
	
	if(ledValue & (1 << 1))
		SIU.GPDO[69].R = 0;
	
	if(ledValue & (1 << 2))
		SIU.GPDO[70].R = 0;
	
	if(ledValue & (1 << 3))
		SIU.GPDO[71].R = 0;
}

void ADJUSTMENTS()
{
	int flag=0,q;

	// 
	if((SIU.PGPDI[2].R & 0x80000000) == 0)
	{
		flag++;
		speedCounter++;
		if(speedCounter > 15)
			speedCounter = 0;
		usingLeds = 1;
		uint4_on_LEDs(speedCounter);
		curveSpeed += 20;
		for(q=0;q<20;q++)
			Delaycamera();
		usingLeds = 0;
	}
	if((SIU.PGPDI[2].R & 0x40000000) == 0)
	{
		flag++;
		pCounter++;
		if(pCounter > 15)
			pCounter = 0;
		usingLeds = 1;
		uint4_on_LEDs(pCounter);
		curveKP += 0.5f;
		for(q=0;q<20;q++)
			Delaycamera();
		usingLeds = 0;
	}
	if((SIU.PGPDI[2].R & 0x20000000) == 0)
	{
		flag++;
		iCounter++;
		
		if(iCounter > 15)
			iCounter = 0;
		usingLeds = 1;
		uint4_on_LEDs(iCounter);
		curveKI += 0.00001f;
		for(q=0;q<20;q++)
			Delaycamera();
		usingLeds = 0;
	}
	if((SIU.PGPDI[2].R & 0x10000000) == 0)
	{
		flag++;
		dCounter++;
		if(dCounter > 15)
			dCounter = 0;
		usingLeds = 1;
		uint4_on_LEDs(dCounter);
		curveKD += 0.5f;
		for(q=0;q<20;q++)
			Delaycamera();		
		usingLeds = 0;
	}
}

void finishLine_isr(void)
{
	if(tickFinishLine)
	{
		tickFinishLine--;
	}
	SIU.ISR.R = 0x00FF;
}

int32_t main(void) 
{
    int32_t i = 0;
    
    InitHW();
    
	SIU.PGPDO[2].R |= 0x0f000000;		/* Disable LEDs*/
    
    INTC_InstallINTCInterruptHandler(PIT3_isr,127,2); /* vector127 for PIT3 */
    INTC_InstallINTCInterruptHandler(finishLine_isr,41,3); /* vector41 for eint */
   
    INTC.CPR.R = 0; /* Lower current INTC's priority to start ext.interrupts */
  
    MotorsInit();
    CameraInit();
    
    //init external interrupt
    SIU.IRER.R = 0x00000401;	//interrupt request enable register
    SIU.IFEER.R = 0x00000401; //Interrupt Falling-Edge Event Enable Register 
    SIU.PCR[3].R = 0x0102;
    //------
    
    
    
    InitPIT3();
    EMIOS_0.CH[6].CBDR.R = 0 ; 
	EMIOS_0.CH[7].CBDR.R = 0 ; 
	 
	for(i = 0; i < BUFFERSIZE; i++)
		History[i] = 0; 
	
    for(;;)
    {
    	if(tickFinishLine != 0)
    	{
	     	EMIOS_0.CH[6].CBDR.R = SPEED + BoostLeft; 
			EMIOS_0.CH[7].CBDR.R = SPEED + BoostRight;  
    	}
    	else
    	{
    		EMIOS_0.CH[6].CBDR.R = 0; 
			EMIOS_0.CH[7].CBDR.R = 0 ;
    	}
		//EMIOS_0.CH[6].CBDR.R = 0;
		//EMIOS_0.CH[7].CBDR.R = 0;    
		ADJUSTMENTS();
     	center = CameraReadD();
    }
}




